﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.ComponentModel;
using System.Windows.Forms.VisualStyles;

namespace Checktachment
{
    [System.Runtime.InteropServices.ComVisible(true)]
    public partial class PropertyPageBase : UserControl, Outlook.PropertyPage
    {
        private Outlook.PropertyPageSite site;
        private bool isDirty;
        private VisualStyleRenderer renderer;

        public PropertyPageBase()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            renderer.DrawBackground(e.Graphics, this.Bounds, e.ClipRectangle);
        }

        protected virtual void OnApply()
        {
            Checktachment.Properties.Settings.Default.Pattern = String.Join("|", listBox1.Items.Cast<String>().ToArray());
        }

        protected override void OnLoad(EventArgs e)
        {
            renderer = new VisualStyleRenderer(VisualStyleElement.Tab.Body.Normal);

            listBox1.Items.Clear();
            String[] keywords = Checktachment.Properties.Settings.Default.Pattern.Split('|');
            foreach (String keyword in keywords)
            {
                listBox1.Items.Add(keyword);
            }

            Type type = typeof(UserControl);
            Type oleType = type.Assembly.GetType("System.Windows.Forms.UnsafeNativeMethods+IOleObject");
            if (oleType == null) throw new InvalidOperationException("Could not get 'System.Windows.Forms.UnsafeNativeMethods+IOleObject'.");
            System.Reflection.MethodInfo method = oleType.GetMethod("GetClientSite");
            if (method == null) throw new InvalidOperationException("Could not get method 'IOleObject.GetClientSite'.");
            site = method.Invoke(this, null) as Outlook.PropertyPageSite;
            base.OnLoad(e);
        }

        [Browsable(false)]
        public bool IsDirty
        {
            get { return isDirty; }
            set
            {
                if (isDirty != value)
                {
                    isDirty = value;
                    if (site != null) site.OnStatusChange();
                }
            }
        }

        #region PropertyPage Members
        public void Apply()
        {
            OnApply();
            this.isDirty = false;
        }

        bool Outlook.PropertyPage.Dirty
        {
            get { return this.IsDirty; }
        }

        public virtual void GetPageInfo(ref string HelpFile, ref int HelpContext)
        {
        }
        #endregion

        /// <summary>
        /// Add item to list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 0 && !listBox1.Items.Contains(textBox1.Text))
            {
                listBox1.Items.Add(textBox1.Text);
                IsDirty = true;
            }
        }

        /// <summary>
        /// Delete item from list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && listBox1.SelectedIndex < listBox1.Items.Count)
            {
                try
                {
                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                    IsDirty = true;
                }
                catch (ArgumentOutOfRangeException)
                {
                }
            }
        }
    }
}
