﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Linq;
using Office = Microsoft.Office.Core;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Checktachment
{
    public partial class ThisAddIn
    {
        Outlook.Inspectors inspectors;
        string pattern; // = "anhang|angehängt|anbei finde|attachment|attached";
        Regex rgx;
        int matchesBeforeEdit = 0;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            inspectors = this.Application.Inspectors;
            inspectors.NewInspector += new Microsoft.Office.Interop.Outlook.InspectorsEvents_NewInspectorEventHandler(Inspectors_NewInspector);
            this.Application.ItemSend += new Outlook.ApplicationEvents_11_ItemSendEventHandler(Application_ItemSend);
            this.Application.OptionsPagesAdd += new Outlook.ApplicationEvents_11_OptionsPagesAddEventHandler(Application_OptionsPagesAdd);
        }

        void Application_OptionsPagesAdd(Outlook.PropertyPages Pages)
        {
            PropertyPageBase page = new PropertyPageBase();
            Pages.Add(page, "CheckTachment Configuration");
        }

        void Application_ItemSend(object Item, ref bool Cancel)
        {
            //MessageBox.Show("Checking attachments ...");
            Cancel = false;

            if (Item is Outlook.MailItem)
            {
                Outlook.MailItem mailItem = (Outlook.MailItem)Item;

                // Do nothing if there are attachments
                if (mailItem.Attachments.Count == 0)
                {
                    //MessageBox.Show("No attachments, checking for key words");
                    // No attachments. Scan mail body for key words
                    this.pattern = Checktachment.Properties.Settings.Default.Pattern;
                    this.rgx = new Regex(this.pattern, RegexOptions.IgnoreCase);
                    MatchCollection matches;

                    try
                    {
                        matches = rgx.Matches(mailItem.Body);
                        //MessageBox.Show("Matches on send: " + matches.Count.ToString());
                    }
                    catch (ArgumentNullException)
                    {
                        //MessageBox.Show("Regex arg null exception");
                        // Empty mail
                        return;
                    }

                    // No matches
                    if (matches != null && matches.Count - this.matchesBeforeEdit > 0)
                    {
                        //MessageBox.Show("No matches found");
                        // Matches found, ask user
                        DialogResult result;
                        result = MessageBox.Show("Hast Du eventuell den Anhang vergessen?", "Checktachment", MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            Cancel = true;
                            return;
                        }
                    }

                    //MessageBox.Show("End of attachment check");
                }
            }
        }

        void Inspectors_NewInspector(Microsoft.Office.Interop.Outlook.Inspector Inspector)
        {
            // Get number of keyword matches before the user can edit
            // Workaround for not knowing text that may be quoted ...
            // Will fail if the user deletes quoted text.
            Outlook.MailItem mailItem = (Outlook.MailItem)Inspector.CurrentItem;
            if (mailItem != null)
            {
                try
                {
                    this.pattern = Checktachment.Properties.Settings.Default.Pattern;
                    this.rgx = new Regex(this.pattern, RegexOptions.IgnoreCase);
                    MatchCollection matches = rgx.Matches(mailItem.Body);
                    this.matchesBeforeEdit = matches.Count;
                    //MessageBox.Show("MatchesBeforeEdit: " + matches.Count.ToString());
                }
                catch (ArgumentNullException)
                {
                    // Empty mail
                    return;
                }
            }
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {

        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
